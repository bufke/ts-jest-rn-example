// https://github.com/facebook/jest/issues/2208#issuecomment-264733133

jest.mock("Linking", () => {
    return {
        addEventListener: jest.fn(),
        canOpenURL: jest.fn(),
        getInitialURL: jest.fn(),
        openURL: jest.fn(),
        removeEventListener: jest.fn(),
    };
});
