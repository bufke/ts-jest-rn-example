const colors = {
  merino: "#F6EFE4",
  mineShaft: "#2D2D2D",
  ash: "#C8C3B9",
  swirl: "#D7D2C8",
  white: "#FFFFFF",
  black: "#000000",

  cinnabar: "#E84A37",
  sweetPink: "#FF9FA0",
  cinnabarHover: "#FF9FA0",

  treePoppy: "#F9A01E",
  gold: "#FED600",
  treePoppyHover: "#FED600",

  turquoise: "#32C8C8",
  jaggedIce: "#B9E1D7",
  turquoiseHover: "#B9E1D7",

  ceruleanBlue: "#3365BC",
  cornflower: "#92D8E5",
  ceruleanBlueHover: "#92D8E5",

  tawnyPort: "#641955",
  kobi: "#E79FD0",
  tawnyPortHover: "#E79FD0",
}

export default colors;
