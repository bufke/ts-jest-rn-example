import React, { Component } from "react";
import {
  AppRegistry,
  AsyncStorage,
  StyleSheet,
  Text,
  View,
} from "react-native";

import HomeComponent from "./home/home.component";

export default class MyApp extends Component<{}, null> {
  public render() {
    return (
      <HomeComponent/>
    );
  }
}
