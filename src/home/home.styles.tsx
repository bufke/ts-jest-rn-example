import { StyleSheet } from "react-native";
import colors from "../global/colors";

const homeStyles = StyleSheet.create<any>({
  headlineRow: {
    flexDirection: 'row',
    marginLeft: -25
  },
  headlineImage: {
    width: 185,
    height: 185
  },
  headline: {
    color: colors.mineShaft,
    fontFamily: 'leaguespartan_bold',
    fontSize: 48,
    lineHeight: 43,
    marginLeft: 15,
    marginTop: 25,
    flex: 1
  },
  carouselSlide: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  carouselSlideText: {
    width: 250,
    marginTop: -20,
    color: colors.mineShaft,
    fontFamily: "opensans_regular",
    fontSize: 15,
    lineHeight: 23,
    textAlign: "center"
  }
});

export default homeStyles;
