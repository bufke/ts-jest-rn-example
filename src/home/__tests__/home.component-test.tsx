import "react-native";
import React from "react";
import HomeComponent from "../home.component";

// Note: test renderer must be required after react-native.
import renderer from "react-test-renderer";

it("renders correctly", () => {
  const tree = renderer.create(
    <HomeComponent />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});
