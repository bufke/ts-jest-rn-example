import React, { Component, PropTypes } from "react";


import {
  Button,
  Image,
  StyleSheet,
  Text,
  View,
} from "react-native";

import colors from "../global/colors";
import homeStyles from "./home.styles";

class HomeComponent extends Component<{}, null> {
  public press() {
    console.log("here");
  }

  public render() {
    return (
      <View style={{backgroundColor: '#F6EFE4', flex: 1}}>
        <View style={homeStyles.headlineRow}>
          <Text style={homeStyles.headline}>
            Some title
          </Text>
        </View>
        <View style={{
          height: 175,
        }}>
        </View>
        <Button
          title="Login"
          onPress={this.press}
        />
      </View>
    );
  }
};

export default HomeComponent;
