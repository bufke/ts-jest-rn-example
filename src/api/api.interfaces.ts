/** A generic Django rest framework response object */
export interface IDRFResponse {
  count: number;
  next?: string;
  previous?: string;
  results: any;
}
