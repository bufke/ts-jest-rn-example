export class ApiBaseService {
  private BASE_URL = "https://example.com";
  private JSON_HEADERS: any = {
    "Accept": "application/json",
    "Content-Type": "application/json",
  };

  protected getJson(url: string) {
    url = this.BASE_URL + url;
    return new Promise((resolve, reject) => {
      let method = "GET";
      let headers = this.getJsonAuthHeaders();
      fetch(url, {
        method,
        headers,
      })
        .then((response) => {
          resolve(response.json());
        });
    });
  }

  protected postJson(url: string, data: any): Promise<any> {
    url = this.BASE_URL + url;
    return new Promise((resolve, reject) => {
      let method = "POST";
      let headers = this.getJsonAuthHeaders();
      let body = JSON.stringify(data);
      fetch(url, {
        method,
        headers,
        body,
      })
        .then((response) => {
          return resolve(response.json());
        });
    });
  }

  private getJsonAuthHeaders() {
    let headers = this.JSON_HEADERS;
    let token = this.getToken();
    if (token) {
      headers.Authorization = "Token " + token;
    }
    return headers;
  }

  private getToken() {
    return "faketoken";
  }
}
