/* tslint:disable:no-var-requires */
let fetchMock = require("fetch-mock");

import { ApiBaseService } from "../api.service";


describe("API base services can fetch resources", () => {
  let expectedResp = {token: "something"};
  let api: ApiBaseService;

  beforeEach(() => {
    api = new ApiBaseService();
  });

  afterEach(() => {
    fetchMock.restore();
  });

  it("gets json", (done) => {
    fetchMock.get("*", expectedResp);
    (<any> api).getJson("/test").then((resp: any) => {
      expect(resp).toEqual(expectedResp);
      done();
    });
  });

  it("posts json", (done) => {
    fetchMock.post("*", expectedResp);
    (<any> api).postJson("/test", {meh: "something"}).then((resp: any) => {
      expect(resp).toEqual(expectedResp);
      done();
    });
  });

  it("expects json from server", (done) => {
    fetchMock.get("*", "junk");
    (<any> api).getJson("/test").then((resp: any) => {
      console.error("shouldn't run");
    })
    .catch((err: any) => {
      done();
    });
  });
});
