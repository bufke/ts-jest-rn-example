"use strict";

import { AppRegistry } from "react-native";
import MyApp from "./build";

AppRegistry.registerComponent("SwoonMobile", () => MyApp);
